﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Moq;
using Moq.Language;
using SportsStore.Infrastructure;
using SportsStore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SportsStore.Tests
{
    public class PageLinkTagHelperTests
    {
        [Fact]
        public async void Can_Generate_Page_Links()
        {
            // Организация
            var pageModel = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10,
            };

            ArrangeInfrastructure arrange = GetInfrastructure(
                pageModel: pageModel,
                pageAction: "Test",
                sequenceActions: new[] { "Test/Page1", "Test/Page2", "Test/Page3" }
            );

            // Действие
            await arrange.Helper.ProcessAsync(arrange.HelperContext, arrange.HelperOutput);

            // Утверждение
            Assert.Equal(@"<a href=""Test/Page1"">1</a>"
                + @"<a href=""Test/Page2"">2</a>"
                + @"<a href=""Test/Page3"">3</a>",
                arrange.HelperOutput.Content.GetContent()
            );
        }

        [Fact]
        public async void Can_Generate_Previous_Arrow()
        {
            // Организация
            var pageModel = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 20,
                ItemsPerPage = 10,
            };

            ArrangeInfrastructure arrange = GetInfrastructure(
                pageModel: pageModel,
                pageAction: "Test",
                sequenceActions: new[] { "Test/Page1", "Test/Page1", "Test/Page2" },
                enableArrows: true
            );

            // Действие
            await arrange.Helper.ProcessAsync(arrange.HelperContext, arrange.HelperOutput);

            // Утверждение
            Assert.Equal(@"<a href=""Test/Page1"" style=""font-weight: bold;"">&lt;</a>"
                + @"<a href=""Test/Page1"">1</a>"
                + @"<a href=""Test/Page2"">2</a>",
                arrange.HelperOutput.Content.GetContent()
            );
        }

        [Fact]
        public async void Can_Generate_Next_Arrow()
        {
            // Организация
            var pageModel = new PagingInfo
            {
                CurrentPage = 1,
                TotalItems = 20,
                ItemsPerPage = 10,
            };

            ArrangeInfrastructure arrange = GetInfrastructure(
                pageModel: pageModel,
                pageAction: "Test",
                sequenceActions: new[] { "Test/Page1", "Test/Page2", "Test/Page2" },
                enableArrows: true
            );

            // Действие
            await arrange.Helper.ProcessAsync(arrange.HelperContext, arrange.HelperOutput);

            // Утверждение
            Assert.Equal(@"<a href=""Test/Page1"">1</a>"
                + @"<a href=""Test/Page2"">2</a>"
                + @"<a href=""Test/Page2"" style=""font-weight: bold;"">&gt;</a>",
                arrange.HelperOutput.Content.GetContent()
            );
        }

        [Fact]
        public async void Can_Generate_Navigation_Form()
        {
            var pageModel = new PagingInfo
            {
                CurrentPage = 1,
                TotalItems = 20,
                ItemsPerPage = 10,
            };

            ArrangeInfrastructure arrange = GetInfrastructure(
                pageModel: pageModel,
                pageAction: "Test",
                sequenceActions: new[] { "Test/Page1", "/" },
                maxPagesLine: 1
            );

            // Действие
            await arrange.Helper.ProcessAsync(arrange.HelperContext, arrange.HelperOutput);

            // Утверждение
            Assert.Contains(@"<form action=""/"" method=""get"">",
                arrange.HelperOutput.Content.GetContent()
            );
        }

        private static ArrangeInfrastructure GetInfrastructure(PagingInfo pageModel,
            string pageAction, IEnumerable<string> sequenceActions,
            ushort maxPagesLine = 7, bool enableArrows = false)
        {
            var urlHelper = new Mock<IUrlHelper>();
            ISetupSequentialResult<string> sequentialResult =
                urlHelper.SetupSequence(x => x.Action(It.IsAny<UrlActionContext>()));

            foreach (string action in sequenceActions)
            {
                sequentialResult = sequentialResult.Returns(action);
            }

            var urlHelperFactory = new Mock<IUrlHelperFactory>();
            urlHelperFactory.Setup(f => f.GetUrlHelper(It.IsAny<ActionContext>()))
                .Returns(urlHelper.Object);

            var helper = new PageLinkTagHelper(urlHelperFactory.Object)
            {
                PageModel = pageModel,
                PageAction = pageAction,
                MaxPagesLine = maxPagesLine,
                EnableArrows = enableArrows,
            };

            var ctx = new TagHelperContext(
                new TagHelperAttributeList(),
                new Dictionary<object, object>(),
                string.Empty
            );

            var content = new Mock<TagHelperContent>();

            var output = new TagHelperOutput("div",
                new TagHelperAttributeList(),
                async (cache, encoder) =>
                    await Task.FromResult(content.Object)
            );

            var arrange = new ArrangeInfrastructure(
                helper,
                ctx,
                output
            );

            return arrange;
        }

        private sealed class ArrangeInfrastructure
        {
            public PageLinkTagHelper Helper { get; }
            public TagHelperContext HelperContext { get; }
            public TagHelperOutput HelperOutput { get; }

            public ArrangeInfrastructure (PageLinkTagHelper helper,
                TagHelperContext helperContext, TagHelperOutput helperOutput)
            {
                Helper = helper;
                HelperContext = helperContext;
                HelperOutput = helperOutput;
            }
        }
    }
}