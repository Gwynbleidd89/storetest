﻿using Moq;
using SportsStore.Controllers;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System.Linq;
using Xunit;
using MockQueryable.Moq;
using Microsoft.AspNetCore.Mvc;

namespace SportsStore.Tests
{
    public class ProductControllerTests
    {
        [Fact]
        public async void Can_Send_Pagination_View_Model()
        {
            // Организация
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductID = 1, Name = "P1" },
                new Product { ProductID = 2, Name = "P2" },
                new Product { ProductID = 3, Name = "P3" },
                new Product { ProductID = 4, Name = "P4" },
                new Product { ProductID = 5, Name = "P5" }
            }.AsQueryable().BuildMock().Object);

            var controller =
                new ProductController(mock.Object) { PageSize = 3 };

            // Действие
            var result = (await controller.List(null, 2)).ViewData.Model as ProductsListViewModel;

            // Утверждение
            PagingInfo pageInfo = result.PagingInfo;
            Assert.Equal(2, pageInfo.CurrentPage);
            Assert.Equal(3, pageInfo.ItemsPerPage);
            Assert.Equal(5, pageInfo.TotalItems);
            Assert.Equal(2, pageInfo.TotalPages);
        }

        [Fact]
        public async void Can_Paginate()
        {
            // Организация
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductID = 1, Name = "P1" },
                new Product { ProductID = 2, Name = "P2" },
                new Product { ProductID = 3, Name = "P3" },
                new Product { ProductID = 4, Name = "P4" },
                new Product { ProductID = 5, Name = "P5" },
            }.AsQueryable().BuildMock().Object);

            var controller =
                new ProductController(mock.Object) { PageSize = 3 };

            // Действие
            var result = (await controller.List(null, 2)).ViewData.Model as ProductsListViewModel;

            // Утверждение
            Product[] prodArray = result.Products.ToArray();
            Assert.True(prodArray.Length == 2);
            Assert.Equal("P4", prodArray[0].Name);
            Assert.Equal("P5", prodArray[1].Name);
        }

        [Fact]
        public async void Filter_Products()
        {
            // Организация
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductID = 1, Name = "P1", Category = "Cat1"},
                new Product { ProductID = 2, Name = "P2", Category = "Cat2"},
                new Product { ProductID = 3, Name = "P3", Category = "Cat1"},
                new Product { ProductID = 4, Name = "P4", Category = "Cat2"},
                new Product { ProductID = 5, Name = "P5", Category = "Cat3"}
            }.AsQueryable().BuildMock().Object);

            var controller = 
                new ProductController(mock.Object) { PageSize = 3 };

            // Действие
            var viewModel = (await controller.List("Cat2", 1)).ViewData.Model as ProductsListViewModel;
            Product[] result = viewModel.Products.ToArray();

            // Утверждение
            Assert.Equal(2, result.Length);
            Assert.True(result[0].Name == "P2" && result[0].Category == "Cat2");
            Assert.True(result[1].Name == "P4" && result[0].Category == "Cat2");
        }

        [Fact]
        public async void Generate_Category_Specific_Product_Count()
        {
            // Организация
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductID = 1, Name = "P1", Category = "Cat1" },
                new Product { ProductID = 2, Name = "P2", Category = "Cat2" },
                new Product { ProductID = 3, Name = "P3", Category = "Cat1" },
                new Product { ProductID = 4, Name = "P4", Category = "Cat2" },
                new Product { ProductID = 5, Name = "P4", Category = "Cat3" }
            }.AsQueryable().BuildMock().Object);

            var target = new ProductController(mock.Object);
            target.PageSize = 3;

            ProductsListViewModel GetModel(ViewResult result) =>
                result.ViewData.Model as ProductsListViewModel;

            // Действие
            int res1 = GetModel(await target.List("Cat1")).PagingInfo.TotalItems;
            int res2 = GetModel(await target.List("Cat2")).PagingInfo.TotalItems;
            int res3 = GetModel(await target.List("Cat3")).PagingInfo.TotalItems;
            int resAll = GetModel(await target.List(null)).PagingInfo.TotalItems;

            // Утверждение
            Assert.Equal(2, res1);
            Assert.Equal(2, res2);
            Assert.Equal(1, res3);
            Assert.Equal(5, resAll);
        }
    }
}
