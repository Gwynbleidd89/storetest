﻿using Xunit;
using Moq;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using SportsStore.Models;
using System.Linq;
using SportsStore.Components;
using SportsStore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace SportsStore.Tests
{
    public class NavigationMenuViewComponentTests
    {
        [Fact]
        public void Can_Select_Categories()
        {
            // Организация
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductID = 1, Name = "P1", Category = "Apples" },
                new Product { ProductID = 2, Name = "P2", Category = "Apples" },
                new Product { ProductID = 3, Name = "P3", Category = "Plums" },
                new Product { ProductID = 4, Name = "P4", Category = "Oranges" },
            }.AsQueryable);

            var target = new NavigationMenuViewComponent(mock.Object);

            // Действие
            var viewComp = target.Invoke() as ViewViewComponentResult;
            string[] results = (viewComp.ViewData.Model as CategoryInfoViewModel).Categories.ToArray();

            // Утверждение
            Assert.True(new[] { "Apples", "Oranges", "Plums" }.SequenceEqual(results));
        }

        [Fact]
        public void Indicates_Selected_Category()
        {
            // Организация
            var categoryToSelect = "Apples";
            var mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product { ProductID = 1, Name = "P1", Category = "Apples" },
                new Product { ProductID = 2, Name = "P2", Category = "Oranges" }
            }.AsQueryable);

            var target = new NavigationMenuViewComponent(mock.Object);
            target.ModelState.SetModelValue("category", categoryToSelect, categoryToSelect);

            // Действие
            var model = (target.Invoke() as ViewViewComponentResult)
                .ViewData.Model as CategoryInfoViewModel;

            // Утверждение
            Assert.Equal(categoryToSelect, model.CurrentCategory);
        }
    }
}
