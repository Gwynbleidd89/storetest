﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsStore.Models;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductRepository _repository;
        public int PageSize = 4;

        public ProductController(IProductRepository repo)
        {
            _repository = repo;
        }

        public async Task<ViewResult> List(string category, uint page = 1)
        {
            if (page == 0)
            {
                page = 1;
            }

            bool categoryPresent = false;

            if (!string.IsNullOrWhiteSpace(category))
            {
                categoryPresent = await _repository.Products
                    .Select(p => p.Category)
                    .ContainsAsync(category);
            }

            IQueryable<Product> products = _repository.Products;

            if (categoryPresent)
            {
                products = products.Where(p => p.Category == category);
            }

            int totalItems = await products.CountAsync();
            int skipCount = (int)(page - 1) * PageSize;

            products = products
                .OrderBy(p => p.ProductID)
                .Skip(skipCount)
                .Take(PageSize);

            var pagingInfo = new PagingInfo
            {
                CurrentPage = (int)page,
                ItemsPerPage = PageSize,
                TotalItems = totalItems
            };

            var productListViewModel = new ProductsListViewModel
            {
                Products = await products.ToArrayAsync(),
                PagingInfo = pagingInfo,
                Category = category
            };

            return View(productListViewModel);
        }
    }
}