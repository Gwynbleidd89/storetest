﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using SportsStore.Models.ViewModels;
using System.Linq;

namespace SportsStore.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private readonly IProductRepository _repository;
        
        public NavigationMenuViewComponent(IProductRepository repo)
        {
            _repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            var catInfo = new CategoryInfoViewModel
            {
                Categories = _repository.Products.Select(x => x.Category)
                    .Distinct()
                    .OrderBy(x => x),
                CurrentCategory = ModelState["category"]?.AttemptedValue
            };

            return View(catInfo);
        }
    }
}
