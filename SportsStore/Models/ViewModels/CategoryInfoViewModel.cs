﻿using System.Collections.Generic;

namespace SportsStore.Models.ViewModels
{
    public class CategoryInfoViewModel
    {
        public IEnumerable<string> Categories { get; set; }
        public string CurrentCategory { get; set; }
    }
}
